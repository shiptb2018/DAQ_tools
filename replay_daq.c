/* replay_daq.c
 *
 * Program to read rawdata files written in the standard SHiP TB format
 * and replay it, sending commands and data to Dispatcher

 * PG June 2018
 */

#define _BCD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <dispatch.h>
#include <getopt.h>
extern char *optarg;                  // from getopt
extern int optind, opterr, optopt;    // from getopt


#define SOR_FLAG 0xFF005C01
#define EOR_FLAG 0xFF005C02
#define SOS_FLAG 0xFF005C03
#define EOS_FLAG 0xFF005C04
#define SPC_FRME 0xFF005C00

char *spFrame[4] = {"SoR","EoR","SoS","EoS"}; 
enum specFr {SoR=0, EoR, SoS, EoS}; 
char *Tag[6]=
    {"RAW_0800","RAW_0802","RAW_0802","RAW_0900","RAW_0B00","RAW_0C00"};
char *pName[6]=
    {"Pix0", "Pix1", "Pix2", "SciF", "RPC","DT"};


typedef struct {
  uint16_t sz;
  uint16_t partId;
  uint32_t cycleId;
  uint32_t frTime;
  uint16_t tExtent;
  uint16_t flags;
} DataFrame_t;

DataFrame_t frame, *fr;
uint16_t buf[1000000];
char *partList=NULL;
long nFrames=0;

int  cLen=5, sLen=4, sMarg=40000, sTrg=3; // use sTrg=2 to slow down a bit, =1 delay even more; =0 fast
bool sendCmd=false, sendAck=false, sendDone=false, sendSpec=true, sendData=true;
bool skipToEoR=false;

char *CycleIdToDate (uint32_t cycle);
int partStat(DataFrame_t *frame, int mode);
int partN(uint16_t partId);

void printHelp (void) {
  printf(
	 "\nUsage: "
	 "replay_daq file_name hostname send_list [options] , where\n"
	 "    send_list is a \"-delimited string with a blank-separated list\n"
	 "              of optional keywords: "
	 " DT RPC SciFi Pix0 Pix1 Pix2 AllDet CMD ACK DONE nodata nospec\n"
	 "    options: -n... - number of spill to send (defaul: all)\n"
	 "             -c... - cycle length (s) (default: 5s) \n"
	 "             -s... - spill length (s) (default: 4s) \n"
         "             -h    - this help\n");
}

int main(int argc, char *argv[]) {
  char *file=NULL, *host=NULL, list[100];
  int nRuns=0, firstRun=-1, lastRun=-1;
  int nCyc=0, firstCyc=-1, lastCyc=-1;
  bool prt_special=false;
  bool inspill=false, inrun=false;
  int i;
  int Ncycles=99999999, Ntriggers=99999999, ntr_spill=0;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  int opt;
  while ((opt = getopt(argc, argv, "n:c:s:?")) != -1) {
    switch (opt) {
    case 'n':  Ncycles = atoi(optarg);                     break;
    case 'c':  cLen = atof(optarg);                        break;
    case 's':  sLen = atof(optarg);                        break;
    case '?':  printHelp();                                break;
                                                           exit(0);                                        
    default :  fprintf(stderr,"invalid option %c\n",opt);  break;
    }
  }

  i = argc-optind; // # of positional args
  if(i==0) {
    printHelp();
    exit(0);
  }
  if(i>0) file=argv[optind];
  if(i>1) host=argv[optind+1];
  if(i>2) partList=argv[optind+2];
  else  partList = "AllDet"; 
  
  printf("\nRead file %s, send to %s, selection list: %s\n",
	 file, host==NULL?"local":host, partList);

  if (partList !=NULL) {
    if (strstr(partList,"nodata") != NULL) sendData=false;
    if (strstr(partList,"nospec") != NULL) sendSpec=false;
    if (strstr(partList,"CMD")    != NULL) sendCmd=true;
    if (strstr(partList,"ACK")    != NULL) sendAck=true;
    if (strstr(partList,"DONE")   != NULL) sendDone=true;
  }

  assert (init_disp_link(host,"w dummy")>0) ; // pure sender
  my_id("Replay_DAQ");

  partStat(NULL,0);

  int fd=open(file,O_RDONLY), nbf, data_sz,nr;
  assert(fd>=0);

  // loop over frames
  while((nbf=read(fd,&frame,16))>0) {
    nFrames++;
    assert(nbf==16);
    data_sz=frame.sz-16;
    if(data_sz>0) {
      nr=read(fd,buf+16,data_sz); // read special frame data
      assert(nr==data_sz);
    }
    /* skip the frames  not selected by the cmd line arg */
    if(partN(frame.partId)<0) continue;

    memcpy(buf,(void *)&frame,16);

    if((frame.frTime & SPC_FRME) == SPC_FRME ) {

      int frtype=(frame.frTime & 0xF)-1;
      
      if (prt_special) {
	printf("--%s: sz=%d partId=%X cycleId=%08X frTime=%x tExtent=%u"
	       " flags=%x data_sz=%d",
	       spFrame[frtype],frame.sz, frame.partId, 
	       frame.cycleId, frame.frTime, frame.tExtent, 
	       frame.flags,data_sz
	       );
	if(data_sz>0)  printf(" %s",(char *)buf+16);
	printf("\n");
      }

      if(frtype == SoR) { // ----------------------------------------------------- SoR
        if(frame.cycleId != lastRun) nRuns++;	
	lastRun=frame.cycleId;
	if(firstRun==-1) {
	  printf("--------------------------- start run %d\n",lastRun);
	  firstRun=lastRun;
	}
	if(!inrun) {
	  sprintf(list,"SoR %d", lastRun);
	  if(sendCmd) put_fullstring("DAQCMD",list);
	  sleep(1);
	  inrun=true;
	  inspill=false;
	}
	usleep(10);
	if(sendAck) {
	  sprintf(list,"SoR %04X", frame.partId);
	  put_fullstring("DAQACK",list); usleep(10);
	}
	if(sendSpec) {
	  sprintf(list,"RAW_%04X", frame.partId);
	  put_fulldata(list,buf,frame.sz); usleep(10);
	  usleep(10);
	}
	if(sendDone) {
	  sprintf(list,"RAW %04X", frame.partId);
	  put_fullstring("DAQDONE",list); usleep(10);
	}
	
      } else if(frtype ==SoS) {// ----------------------------------------------------- SoS
        if(frame.cycleId != lastCyc) nCyc++;	
	lastCyc=frame.cycleId;
	if(firstCyc==-1) firstCyc=lastCyc;
	if(nCyc > Ncycles)  skipToEoR=true;
	if(skipToEoR) continue;
	ntr_spill=0;

	if(!inspill) {
	  sleep(cLen);
	  printf("--------------------------- start spill %3d %x\n",nCyc,frame.cycleId);
	  sprintf(list,"SoS %d", lastCyc);
	  if(sendCmd) put_fullstring("DAQCMD",list);
	  usleep(40000);
	  inspill=true;
	}
	usleep(10);
	sprintf(list,"SoS   %04X", frame.partId);
	if(sendAck) put_fullstring("DAQACK",list); usleep(10);
	if(sendSpec) {
	  sprintf(list,"RAW_%04X", frame.partId);
	  put_fulldata(list,buf,frame.sz); usleep(10);
	}
	memcpy((void *)&frame,buf,16);

      } else if(frtype ==EoS) {// ----------------------------------------------------- EoS
	if(skipToEoR) continue;
	if(inspill) {
	  inspill=false;
	  sprintf(list,"EoS %d", lastCyc);
	  if(sendCmd) put_fullstring("DAQCMD",list);
	  usleep(10);
	}

	sprintf(list,"SoS   %04X", frame.partId);
	if(sendDone) put_fullstring("DAQDONE",list); usleep(10);
	if(sendSpec) {
	  sprintf(list,"RAW_%04X", frame.partId);
	  if(frame.tExtent>Ntriggers+1) frame.tExtent = Ntriggers+1;
	  put_fulldata(list,buf,frame.sz); usleep(10);
	}
	usleep(1000);

	sprintf(list,"EoS %04X", frame.partId);
	if(sendAck) put_fullstring("DAQACK",list); usleep(10);
	usleep(10);
	sprintf(list,"EoS %04X", frame.partId);
	if(sendDone) put_fullstring("DAQDONE",list); usleep(10);

      } else if(frtype ==EoR) {// ----------------------------------------------------- EoR
	skipToEoR=false;
	inspill=false;
	if(inrun) {
	  sprintf(list,"EoR %d", lastRun);
	  if(sendCmd) put_fullstring("DAQCMD",list);
	  usleep(100);
	  inrun=false;
	  inspill=false;
	}
	sprintf(list,"EoR %04X", frame.partId);
	if(sendAck) put_fullstring("DAQACK",list); usleep(10);
	usleep(100);
	if(sendSpec) {
	  sprintf(list,"RAW_%04X", frame.partId);
	  put_fulldata(list,buf,frame.sz); usleep(10);
	}
	sprintf(list,"EoR %04X", frame.partId);
	if(sendDone) put_fullstring("DAQDONE",list); usleep(10);
      }
    } else { //--------------------------------------------------------------- data frame
      if(skipToEoR) continue;
      ntr_spill++;
      int partn=partStat(&frame,1);  // fill stats
      if(sendData) {
	int rc=put_fulldata(Tag[partn],buf,frame.sz);
	assert(rc>=0);
	if(sTrg>0 && !(random()%sTrg)) usleep(sTrg);
      }
    }
  }
  if(nbf==0) {
    close(fd);
    printf("EoF reached after %ld frames\n",nFrames);
    printf("Total: %d runs: first %d, last %d\n",nRuns,firstRun,lastRun);
    printf("Total: %d cycles: first %d (",   nCyc,firstCyc);
    printf("%s) ",CycleIdToDate(firstCyc));
    printf("last %d(%s)\n", lastCyc,CycleIdToDate(lastCyc));
    partStat(NULL,2);
  } else {
    perror("Error reading file");
  }  
    close(fd);
    return 0;
}

	 
char *CycleIdToDate (uint32_t cycle) {
 static time_t time_offset=-1, cycTime;
  static char s[200];
  if((long int) time_offset == -1) {
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of Apr 2015, 12:00 am
    time_offset = mktime(&tp);
  }
  cycTime = time_offset + (time_t)(cycle/5);
  struct tm ts = *localtime(&cycTime);
  strftime(s,199,"%d/%m/%Y- %X",&ts);
  return s;
}
  
struct part {
  uint32_t minTstmp;
  uint32_t maxTstmp;
  uint16_t maxTrigN;
  uint16_t minFrSize;
  uint16_t maxFrSize;
  uint32_t frames;
  uint32_t sent;
} Part[5];

int partStat(DataFrame_t *frame, int mode) {
  int i, partn;
  if(mode==0) {
    for(i=0;i<6;i++) {
      Part[i].minTstmp=0xFFFFFFF;
      Part[i].maxTstmp=0;
      Part[i].maxTrigN=0;
      Part[i].minFrSize=0xFFFF;
      Part[i].maxFrSize=0;
      Part[i].frames=0;
      Part[i].sent=0;
    }
  } else if(frame != NULL) {

    uint16_t  partId=frame->partId;
    partn = partN(partId);
    //char *partName=pName[partn];

#ifdef PRT_DATA
    uint16_t 
      frSize = frame->sz,
      cycId=frame->cycleId,
      trNum=frame->tExtent, 
      flags=frame->flags;
    uint32_t
      frTime=frame->frTime; 
    int data_sz = frame->sz-sizeof(DataFrame_t);
    if((partList !=NULL) && (strstr(partList,partName) != NULL)) {
      printf("----%s sz=%d partId=%X cycleId=%08X frTime=%x"
	     " tExtent=%u flags=%x data_sz=%d\n",
	     pName[hash[partX]],frame->sz, 
	     frame->partId, frame->cycleId, frame->frTime, 
	     frame->tExtent, frame->flags,data_sz);
    }
#endif
    Part[partn].frames++;
    if(frame->frTime < Part[partn].minTstmp ) Part[partn].minTstmp=frame->frTime;
    if(frame->frTime > Part[partn].maxTstmp ) Part[partn].maxTstmp=frame->frTime;
    if(frame->tExtent > Part[partn].maxTrigN ) Part[partn].maxTrigN=frame->tExtent;
    if(frame->sz < Part[partn].minFrSize ) Part[partn].minFrSize=frame->sz;
    if(frame->sz > Part[partn].maxFrSize ) Part[partn].maxFrSize=frame->sz;
    if(sendData) Part[partn].sent++;
    return partn;
  }
  else if(mode ==2 ) { // print statistics
    printf("------- Partition stats -------------\n");
    for(i=0;i<6;i++) {
      if(Part[i].frames>0) {
	printf("%4s  tot frames: %d; frTime: %d/%d; frSize: %d/%d;  Max tr#: %d Triggers sent: %d\n",
	       pName[i],
	       Part[i].frames,
	       Part[i].minTstmp,
	       Part[i].maxTstmp,
	       Part[i].minFrSize,
	       Part[i].maxFrSize,
	       Part[i].maxTrigN,
	       Part[i].sent);
	    
      }
    }
  }
  return -1;
}

int partN(uint16_t partId) {
  static unsigned char hash[65]; // 65 = 0xC0-0x80  + 1
  static int mode=0;
  int i, partn;
  unsigned char partX = ((partId&(0xF)) | ((partId>>4)&(0xF0)))-0x80;
  static bool pEnabled[6];

  if(mode==0) {

    //    printf("partN: partList=|%s|\n", partList);

    memset(hash,0,65);
    hash[0]=0;  // Pix0 (0x800)
    hash[1]=1;  // Pix2 (0x801)
    hash[2]=2;  // Pix2 (0x802)
    hash[16]=3; // SciFi (0x900)
    hash[48]=4; // RPC (0xB00) 
    hash[64]=5; // DT  (0xC00)
    mode=1;

    for(i=0;i<6;i++) {
      pEnabled[i]=false;
      if( ( partList !=NULL) && 
	  ((strstr(partList,pName[i]) != NULL) || (strstr(partList,"AllDet") != NULL) ) )
	  pEnabled[i]=true;
	  printf("partN: i=%d %4s pEnabled=%d\n", i,pName[i],pEnabled[i]);
    }
  }

  assert(partX<=64);
  partn=hash[partX];
  assert(partn<=5);
  return pEnabled[partn]? partn: -1;
}   

     
  
  
