
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <string.h>

char *cycleId2time( uint32_t cycleId) {

  static time_t time_offset=-1, time_now;
  static char s[200]; 
  struct tm ts;
 ;

  if((long int) time_offset==-1) {
    char s[200];
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of April 2015, 12:00 am
    strftime(s,199,"Time offset date: %x time %X",&tp);
    //printf("+++++%s\n",s);
    time_offset=mktime(&tp);
  }
  time_now=cycleId/5 + time_offset;
  ts = *localtime(&time_now);
  strftime(s,199,"%d%m%y_%H%M%S",&ts);
  return s;
}

uint32_t time2cycleId(time_t tt) {
  /*
   * Cycle ID generator, according to MJ+PG "Raw data format" memo
   * mode: =0, use OS clock; =1, (not yet implemented) use external id from the central DAQ message 
   * Author: PG, Jan 2018
   */

  static time_t time_offset=-1, time_now;
  static char s[200]; 
  struct tm ts;
  uint32_t cycleId;

  if((long int) time_offset==-1) {
    char s[200];
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of April 2015, 12:00 am
    strftime(s,199,"Time offset date: %x time %X",&tp);
    //printf("+++++%s\n",s);
    time_offset=mktime(&tp);
  }
  //time_now=time(NULL);
  time_now=tt;
  ts = *localtime(&time_now);
  strftime(s,199,"%d%m%y_%H%M%S",&ts);
 
  cycleId = difftime(time_now,time_offset)*5;
  printf("getCycleIs:now |%s| cycleId %X \n",
	 s,cycleId);

  return  cycleId;
}

int main(int argc, char *argv[])
{
  //   double start;
   char tag[9], cmd[10], msg[100];
   int rc,sz,i, j;
   uint32_t cycleId;
 
   setbuf(stdout, NULL);
   cycleId=   time2cycleId(time(NULL));
   printf(" cycleID=%X  corresponds to |%s|\n",cycleId, cycleId2time(cycleId));
   return 0;
}
