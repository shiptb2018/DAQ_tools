/* cmd_watch.c
 *
 * Program to monitor the DAQ traffic, with options to read full messages and data blocks 
 * (or only headers) and record the received data block to a local file
 *
 * Usage:   ./cmd_watch [DispHost [c|d|cd|n  [options]]]
 *
 * The 2nd argument tells what to show on screen (n=nothing). It does not affect
 * the way the data is received.

 * Options: -mxxx  receive only xxx bytes from each frame (xxx>=16)
 *          -ryyy  record all received data frames to file yyy (full path)
 *          -bzzz  (only in combination with -r) frame buffer size in MB. Without
 *                 that option, each frame is immediately written to disk. With -b
 *                 option, the frames are buffered before writing. Recommended mode:
 *                 NO  -b option: let OS do buffering. 
 *
 * Examples: 
 *       ./cmd_watch (without args) 
 *              to watch the local Dispatcher, read only data headers and show commands only.
 *              Standard monitoring run.
 *       ./cmd_watch local c -m5000 -r/data/daq/Run1.dat
 *              Standard way to record received data,
 *       ./cmd_watch shipdaq01 n -m5000
 *              Creates a dummy load on Dispatcher, prints nothing but receives 
 *              the full DAQ traffic
 *              
 *
 * To stop the program, either CTRL-C or send the message STOP to the same dispatcher
 * e.g., sendcmd local STOP
 *
 * PG, last version 16.07.2018:  timestamps are added
 */
#define Version 1607
#define _BSD_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "dispatch.h"
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <getopt.h>
extern char *optarg;                  // from getopt
extern int optind, opterr, optopt;    // from getopt


typedef struct {
  uint16_t size;
  uint16_t partitionID;
  uint32_t cycleID;
  uint32_t frameTime;
  uint16_t timeExtent;
  uint16_t flags; 
} DataFrameHeader;

typedef struct {
  uint16_t channelId;
  uint16_t hitTime;
#ifdef EXTRADATA
  uint16_t extraData;
#endif
} RawDataHit;

typedef struct { 
  DataFrameHeader header;   
  RawDataHit hits[]; 
} DataFrame;


#define SPECIAL_SOS   0x5C03
#define SPECIAL_EOS   0x5C04
#define SPC_FRME 0xFF005C00


char *buf;   // event buffer (normally, 3000 B is enougth)
char *sbuf=NULL; // spill buffef
long nBrecorded,records=0,nbuffered=0,sbufSize=0;  // spill buffer size (B)
bool prt_s=false, prt_d=false, prt_c=false;
FILE *outfile=NULL;
char *Self; // the program name
char fname[100]={""};

void signal_handler (int signum);
void endAll         (int exitcode);
void dumpBuf        (char *b, long nB);

int main(int argc, char *argv[])
{
  char opt, tg[TAGSIZE+1];
  char *host = (char *)0, list[200]; 
  char DataTags[200] =
    {"a RAW_0800 a RAW_0801 a RAW_0802 a RAW_0C00 a RAW_0B00 a RAW_0900 a RAW_8100"}; 
  char CmdTags[200] =
    {"a STOP a DAQCMD a DAQACK a DAQDONE a TrgCMD a TrgINFO a EvbCMD a EvbACK a EvbDONE"};

  DataFrame *fr;
  int rc, rw, sz;
  long kcmd=0, kdata=0, Breceived=0;
  int maxdata=16; // max received data blocks len (by default, 16B = frame header only) 
  uint16_t evsz;
  //uint16_t options;
  bool specFr, recording=false;
  struct tm ts;
  time_t time_now;
  char tstamp[200];

  Self = argv[0];
  
  setbuf(stdout,NULL); // no buffering in stdout and stderr
  setbuf(stderr,NULL);

  signal(SIGINT,  signal_handler);
  signal(SIGABRT, signal_handler);


  // Dispatcher host name
  if(argc>1 && strcmp(argv[1],"local")!=0)  host=argv[1];
  if(argc>2 && strchr(argv[2],'d')!=NULL) prt_d = true;
  if(argc>2 && strchr(argv[2],'c')!=NULL) prt_c = true;
  if(argc>2 && strchr(argv[2],'s')!=NULL) prt_s = true;
  
  while ((opt = getopt(argc, argv, "m:b:r:")) != -1) {
    switch (opt) {
    case 'm':  maxdata = atoi(optarg)<sizeof(DataFrameHeader)?
	sizeof(DataFrameHeader) : atoi(optarg);            break;
    case 'r':  {
      strncpy(fname,optarg,sizeof(fname)-1); 
      recording=true;                                      break; 
    }
      //case 'o':  options = atoi(optarg);                     break; 
    case 'b':  sbufSize = atoi(optarg)*1000*1000;          break; // -b (MB)
    default :  fprintf(stderr,"invalid option %c\n",opt);  break;
    }
    assert(errno != ERANGE);
  }
  
  buf = malloc(maxdata+1);
  if(buf == NULL) {
    fprintf(stderr,"Error: cannot malloc ev buf of %dB\n",maxdata);
    exit(1);
  } 
  memset(buf,0,maxdata+1);

  if(sbufSize >0 && sbufSize > maxdata) {
    sbuf = malloc(sbufSize);
    if(sbuf == NULL) {
      fprintf(stderr,"Error: cannot malloc spill buf of %ld MB\n",sbufSize/1000000);
      exit(1);
    } 
  }
  memset(sbuf,0,sbufSize);

  if(recording) {
    outfile = fopen(fname,"w");
    if (outfile == NULL) {
      fprintf(stderr,"Error: cannot open file %s for writing\n",fname);
      exit(1);
    }
  }    
    
  if(host==NULL) strcpy(list, "local host");
  else strncpy (list, host, sizeof(list)-1);

  rc=init_2disp_link(host,DataTags, CmdTags);

  if( rc < 0 ) {
    fprintf(stderr," failed to connect to %s\n",list);
    exit(1); 
  }
  printf("Successfully connected to %s; display cmds/data : %c/%c maxdata=%dB\n",
	 list, prt_c?'t':'f', prt_d?'t':'f',maxdata);
  if(recording) 
    printf("Recording to %s, sbuf size: %ldB, buffering: %c", 
	   fname,sbufSize, sbufSize>maxdata?'t':'f');
  printf("\n");

  send_me_always();
  my_id("cmd_watch");
 
  while ((rw=wait_head(tg,&sz)) > 0) {
    
    if(strstr("TrgCMD TrgINFO EvbCMD EvbACK EvbDONE DAQCMD DAQACK DAQDONE",tg) != NULL) {
      if((rc = get_string(list,sizeof(list))) < 0) {
	printf("Err: rc=get_string=%d. tag=%s sz=%d\n",rc,tg,sz);
	exit(3);
      }
      kcmd++;
      Breceived += sz;

      // timestamp 16.07.2018 PG
      time_now=time(NULL);   
      ts = *localtime(&time_now);   
      strftime(tstamp,199,"%d%m%y_%H%M%S",&ts);
      if(prt_c) {
	printf("-- [%s] cmd tag=%s sz=%d \t %s (cmd %ld/data %ld). Tot recvd: %8.2f MB",
	       tstamp, tg,sz,list, kcmd,kdata,(double)Breceived*1.E-6); 
	if(recording)
	  printf(" recorded: %8.2f MB (%ld records)",(double)nBrecorded*1.E-6,records);
	printf("\n");
      }

    } else if(strstr("STOP",tg) != NULL) {  // stop cmd_watch by a command
      endAll(0);

    } else {
      if((rc = get_data(buf,maxdata)) < 0) {
	printf("Err: rc=get_data=%d. tag=%s sz=%d (cmd %ld/data %ld)\n",rc,tg,sz,kcmd,kdata);
	exit(3);
      }
      kdata++;
      evsz = sz>maxdata? maxdata: sz;
      Breceived += evsz;
      fr = (DataFrame *) buf;

      specFr=(fr->header.frameTime & SPC_FRME) == SPC_FRME;

      if(prt_d || (prt_s && specFr))
	 printf("   data tag=%s rc/sz=%d/%d \t part %04X cyc %X time %X/%d ext %d fl %x \n",
	     tg,rc,sz, 
	     fr->header.partitionID,
	     fr->header.cycleID,
	     fr->header.frameTime, fr->header.frameTime,
	     fr->header.timeExtent,
	     fr->header.flags);
      /* recording */
      if(recording) {
	long rlen=evsz;
	fr->header.size = evsz;  // truncate recorded events, if needed
	if(sbuf == NULL) {  // no buffering, write every single ev
	  dumpBuf(buf,rlen);
	} else { // buffering
	  if(nbuffered + evsz > sbufSize) { // dump if sbuf is full  
	    dumpBuf(sbuf,nbuffered);
	    nbuffered = 0;
	  }
	  memcpy(sbuf+nbuffered,buf,evsz);
	  nbuffered += evsz;
	}
      }
    }
  }
  return 0;
}
/*
 * signal callback (used to intercept Ctrl-C and abort signals 
 */
void signal_handler(int signum) {
  printf("\n%s is closing on  %d signal \n",Self, signum);
  endAll(signum);
}
void endAll(int exitcode) {
  if(outfile != NULL) {
    if( sbuf != NULL && nbuffered>0) {
      fwrite(sbuf,2,nbuffered/2,outfile);
      printf("Flush %ld B remaining in spill buf\n",nbuffered);
    }
    printf("Closing output file %s \n", fname);
    fclose(outfile);
  }
  exit (exitcode);
}

void dumpBuf(char *b, long nB) {
  long nwr=fwrite(b,2,nB/2,outfile);
  records++;
  if(nwr != nB/2) {
    fprintf(stderr,"Write err: rec %ld expected %ld words, written %ld\n",
	    records, nB/2, nwr);
    endAll(100);
  }
  nBrecorded += nB;
}

	 
   
