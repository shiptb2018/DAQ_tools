/* rd_rawfile.c
 *
 * Program to read rawdata files written in the standard SHiP TB format
 * and compile some elementary data statistics
 *
 * It is a very simple code, consisting mostly of prints.
 * Without prints and statistics, the code shrinks to:

 DataFrame_t frame, *fr;
 uint16_t buf[1000000];
 int fd=open(file,O_RDONLY), nbf, data_sz,nr;
  assert(fd>=0);

  // loop over frames
  while((nbf=read(fd,&frame,16))>0) {
    assert(nbf==16);
    data_sz=frame.sz-16;
      if(data_sz>0) {      // skip data part
	nr=read(fd,buf,data_sz);
	assert(nr==data_sz);
      }
    }
  }
  if(nbf==0) {
     close(fd);
  }

 *
 * Usage: ./rd_rawfile full_data_file_path [part_list]
 *    where "part_list" is a string with the list of frames to print, eg
 *    ./rd_rawfile /data/daq/Run1.dat "Pix0 Pix2 DT SPEC" 
 *    to print the headers of all special and Pix1, Pix2 and DT data frames.
 *    All possible names: SPEC, Pix0, Pix1, Pix2, DT, RPC and SciF
 *
 * PG June 2018
 */

#define _BCD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define SOR_FLAG 0xFF005C01
#define EOR_FLAG 0xFF005C02
#define SOS_FLAG 0xFF005C03
#define EOS_FLAG 0xFF005C04
#define SPC_FRME 0xFF005C00

char *spFrame[4] = {"SoR","EoR","SoS","EoS"}; 

typedef struct {
  uint16_t sz;
  uint16_t partId;
  uint32_t cycleId;
  uint32_t frTime;
  uint16_t tExtent;
  uint16_t flags;
} DataFrame_t;

DataFrame_t frame, *fr;
uint16_t buf[1000000];
char *partList=NULL;
long nFrames=0;

void partStat(DataFrame_t *fd, int mode);
char *CycleIdToDate (uint32_t cycle);

int main(int argc, char *argv[]) {
  char *file=argv[1];
  int nRuns=0, firstRun=-1, lastRun=-1;
  int nCyc=0, firstCyc=-1, lastCyc=-1;
  bool prt_special=false;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  if(argc>2) partList=argv[2];
  prt_special = (partList !=NULL) && (strstr(partList,"SPEC") != NULL);

  partStat(NULL,0);

  int fd=open(file,O_RDONLY), nbf, data_sz,nr;
  assert(fd>=0);

  // loop over superframes
  while((nbf=read(fd,&frame,16))>0) {
    nFrames++;
    assert(nbf==16);
    data_sz=frame.sz-16;
    if((frame.frTime & SPC_FRME) == SPC_FRME ) {
      int frtype=(frame.frTime & 0xF)-1;
      if (prt_special)
	printf("--%s: sz=%d partId=%X cycleId=%08X frTime=%x tExtent=%u"
	       " flags=%x data_sz=%d",
	       spFrame[frtype],frame.sz, frame.partId, 
	       frame.cycleId, frame.frTime, frame.tExtent, 
	       frame.flags,data_sz);

      if(data_sz>0) {
	nr=read(fd,buf,data_sz); // read special frame data
	assert(nr==data_sz);
	if(prt_special) printf(" %s",(char *)buf);
      }
      if(prt_special) printf("\n");

      if(frtype == 1) {// SoR
        if(frame.cycleId != lastRun) nRuns++;	
	lastRun=frame.cycleId;
	if(firstRun==-1) firstRun=lastRun;
      } else if(frtype ==3) {// SoS
        if(frame.cycleId != lastCyc) nCyc++;	
	lastCyc=frame.cycleId;
	if(firstCyc==-1) firstCyc=lastCyc;
      }
	

    } else {               // data frame
      partStat(&frame,1);  // fill stats
      if(data_sz>0) {      // skip hits
	nr=read(fd,buf,data_sz);
	assert(nr==data_sz);
      }
    }
  }
  if(nbf==0) {
    close(fd);
    printf("EoF reached after %ld frames\n",nFrames);
    printf("Total: %d runs: first %d, last %d\n",nRuns,firstRun,lastRun);
    printf("Total: %d cycles: first %d (",   nCyc,firstCyc);
    printf("%s) ",CycleIdToDate(firstCyc));
    printf("last %d(%s)\n", lastCyc,CycleIdToDate(lastCyc))
;
    partStat(NULL,2);
  } else {
    perror("Error reading file");
  }  
    close(fd);
    return 0;
}

struct part {
  uint32_t minTstmp;
  uint32_t maxTstmp;
  uint16_t maxTrigN;
  uint16_t minFrSize;
  uint16_t maxFrSize;
  uint32_t frames;
} Part[5];
    
void partStat(DataFrame_t *frame, int mode) {
  static unsigned char hash[65]; // 65 = 0xC0-0x80  + 1
  static char *pName[6]=
    {"Pix0", "Pix1", "Pix2", "SciF", "RPC ","DT  "};
  char *partName;
  int i, partn;
  if(mode==0) {
    memset(hash,0,65);
    hash[0]=0;  // Pix0 (0x800)
    hash[1]=1;  // Pix2 (0x801)
    hash[2]=2;  // Pix2 (0x802)
    hash[16]=3; // SciFi (0x900)
    hash[48]=4; // RPC (0xB00) 
    hash[64]=5; // DT  (0xC00)
    for(i=0;i<6;i++) {
      Part[i].minTstmp=0xFFFFFFF;
      Part[i].maxTstmp=0;
      Part[i].maxTrigN=0;
      Part[i].minFrSize=0xFFFF;
      Part[i].maxFrSize=0;
      Part[i].frames=0;
    }
  } else if(frame != NULL) {
    uint16_t partId=frame->partId; 

    int data_sz = frame->sz-sizeof(DataFrame_t);
    unsigned char partX= ((partId&(0xF)) | ((partId>>4)&(0xF0)))-0x80;
    assert(partX<=64);
    partn=hash[partX];
    assert(partn<=5);
    partName=pName[partn];
    //printf("%ld %X %x\n",nFrames,partId,partX);

    if((partList !=NULL) && (strstr(partList,partName) != NULL)) {
      printf("----%s sz=%d partId=%X cycleId=%08X frTime=%x"
	     " tExtent=%u flags=%x data_sz=%d\n",
	     pName[hash[partX]],frame->sz, 
	     frame->partId, frame->cycleId, frame->frTime, 
	     frame->tExtent, frame->flags,data_sz);
    }
    Part[partn].frames++;
    if(frame->frTime < Part[partn].minTstmp ) Part[partn].minTstmp=frame->frTime;
    if(frame->frTime > Part[partn].maxTstmp ) Part[partn].maxTstmp=frame->frTime;
    if(frame->tExtent > Part[partn].maxTrigN ) Part[partn].maxTrigN=frame->tExtent;
    if(frame->sz < Part[partn].minFrSize ) Part[partn].minFrSize=frame->sz;
    if(frame->sz > Part[partn].maxFrSize ) Part[partn].maxFrSize=frame->sz;
  }
  else if(mode ==2 ) { // print statistics
    printf("------- Partition stats -------------\n");
    for(i=0;i<6;i++) {
      if(Part[i].frames>0) {
	printf("%s  tot frames: %d; frTime: %d/%d; frSize: %d/%d;  Max tr#: %d\n",
	       pName[i],
	       Part[i].frames,
	       Part[i].minTstmp,
	       Part[i].maxTstmp,
	       Part[i].minFrSize,
	       Part[i].maxFrSize,
	       Part[i].maxTrigN);
      }
    }
  }
}
	 
char *CycleIdToDate (uint32_t cycle) {
 static time_t time_offset=-1, cycTime;
  static char s[200];
  if((long int) time_offset == -1) {
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of Apr 2015, 12:00 am
    time_offset = mktime(&tp);
  }
  cycTime = time_offset + (time_t)(cycle/5);
  struct tm ts = *localtime(&cycTime);
  strftime(s,199,"%d/%m/%Y- %X",&ts);
  return s;
}
  


      
     
      
  
  
